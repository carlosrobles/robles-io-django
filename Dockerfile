FROM python:3

RUN mkdir /app

COPY Pipfile Pipfile.lock manage.py roblesio /app/

WORKDIR /app

RUN pip install pipenv && \
    pipenv install --deploy --system

CMD python manage.py runserver 0.0.0.0:8000
