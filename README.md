# robles-io
Django hello world app

<br>

## What you'll Need
`docker`, `docker-compose`, and `make`

<br>

## Usage
Launch a local instance of the roblesio django project:

`make up`

<br>
View the django welcome page:

http://localhost:8000

<br>

Stop and destroy the container

`make down`
